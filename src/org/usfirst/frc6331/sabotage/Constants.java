package org.usfirst.frc6331.sabotage;


import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Encoder;

/**
 * For this, we are instantiating every single motor. There exists a "Victor" class, for example, but it's
 * in the global library and exists as a usable object. For this we do public static final Object OBJECT_NAME_CUSTOMIZED = new Object(port number);
 * That's about it. Public static final makes it so that you can access this in any function and any class in your code as well as keeping in a non 
 * changeable constant.
 * @author Vlad
 *
 */
public class Constants extends IterativeRobot {
	
	/**
	 * Joystick port
	 */
	public static final Joystick DRIVE_STICK = new Joystick(0);
	public static final Joystick ACTION_STICK = new Joystick(1);
	
	/**
	 * The speed of the robot.
	 */
	public static final double DRIVE_SPEED = 0.6;

	/**
	 * Calling the motors for the drive train.
	 */
	public static final Victor LEFT_BACK_DRIVE = new Victor(0);
    public static final Victor LEFT_FRONT_DRIVE = new Victor(7);
    public static final Victor RIGHT_BACK_DRIVE = new Victor(1);
    public static final Victor RIGHT_FRONT_DRIVE = new Victor(2);
    
    
    /**
     * Calling the motors for the 'claw'.
     */
    //public static final Victor LEFT_CLAW = new Victor(0);
    public static final Victor RIGHT_CLAW = new Victor(3); //7
    
    public static final Victor OPENER = new Victor(10);
    
    /**
     * Calling the motors for the box lift.
     */
    public static final Victor LIFT = new Victor(9);
    
    //public static final DigitalInput LIMIT = new DigitalInput(0);
    /**
     * Calling the motors for the climbing system.
     */
    public static final Victor CLIMB = new Victor(4);
    
    /**
     * Calling the encoders.
     */
   /* public static final Encoder RIGHT_ENCODER = new Encoder(0, 1, false, Encoder.EncodingType.k4X);
    public static final Encoder LEFT_ENCODER = new Encoder(2, 3, false, Encoder.EncodingType.k4X);*/
    
    
}
