package org.usfirst.frc6331.sabotage;


import org.usfirst.frc6331.sabotage.autonomous.Auto; 
import org.usfirst.frc6331.sabotage.mechanics.Claw;
import org.usfirst.frc6331.sabotage.mechanics.Climb;
import org.usfirst.frc6331.sabotage.mechanics.Drive;
import org.usfirst.frc6331.sabotage.mechanics.Lift;


import edu.wpi.first.wpilibj.CameraServer;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;


/**
 * 
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 * @author Vlad
 * 
 */
public class Robot extends IterativeRobot {
	
	
	Command autonomousCommand;
	static SendableChooser autoChooser = new SendableChooser();
	
	
    /**
     * This function is run when the robot is first started up and should be
     * used for any initialization code. For example, in auto we reset the gyros and encoders.
     * This is code you want ran before anything even moves.
     */
	@Override
    public void robotInit() {
		
		
    	CameraServer.getInstance().startAutomaticCapture();
    	//Auto.gyroInit();
    	//CameraServer.getInstance().startAutomaticCapture();
    }
    
    /**
     * This function is called periodically during autonomous. Periodic means it's being constantly called 60 times per second.
     */
    public void autonomousPeriodic() {
    	
    }
    
    /**
     * This initiates an autonomous function only once. Not periodic. For this year, there are game messages, or game data.
     * Since we're only using the switch for auto, we only need the first character, being R or L. The messages are usually something like RLR, LRL
     * RRL, LRR, etc. gameData is the string that is sent to the driver station and charAt(0) is basically the character at the first position (0) of the string.
     * In RLR, R is at 0, L is at 1, and R is at 2. Java starts counting with 0.
     */
    public void autonomousInit() {
    	String gameData;
		gameData = DriverStation.getInstance().getGameSpecificMessage();
		if(gameData != null && gameData.length() >= 1 && gameData.charAt(0) == 'R')
		{ 
			Auto.midRight();
			
			//Auto.longRight();
			//Auto.shortLeft();
			//Auto.middleLeft();
		} else {
			Auto.midLeft();
			//Auto.shortRight();
			//Auto.longLeft();
			//Auto.middleRight();
			
		}
    	//Auto.straightTest();
    	Auto.reset();
    	//Auto.straightGyro();
		
    }

    /**
     * This function is called periodically during operator control. Periodic means it's being constantly called 60 times per second.
     * Every function and every part of your robot should be initialized here. If you have a laser beam, you'd initialize it in here.
     * Since we have drive, claw, lift, and climb, that's what I put.
     */
    public void teleopPeriodic() {
    	Drive.driveInit();
    	Claw.clawInit();
    	Lift.liftInit();
    	Climb.climbInit();
    }
      
}
