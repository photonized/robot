package org.usfirst.frc6331.sabotage.autonomous;

import org.usfirst.frc6331.sabotage.Constants;

import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.Victor;

public class Commands {
	
	static Victor lB = Constants.LEFT_BACK_DRIVE;
	static Victor lF = Constants.LEFT_FRONT_DRIVE;
	static Victor rB = Constants.RIGHT_BACK_DRIVE;
	static Victor rF = Constants.RIGHT_FRONT_DRIVE;
	static Victor claw = Constants.OPENER;
	static Victor r_in = Constants.RIGHT_CLAW;
	static Victor lift = Constants.LIFT;
	
	static SpeedControllerGroup l = new SpeedControllerGroup(lB, lF);
	static SpeedControllerGroup r = new SpeedControllerGroup(rB, rF);
	
	
	/**
	 * 500ms
	 */
	public static void turnLeft() {
		l.set(-0.4);
		r.set(-1.0);
	}
	
	/**
	 * 700ms
	 */
	public static void turnRight() {
		l.set(0.2);
		r.set(0.3);
	}
	
	public static void forward() {
		l.set(0.6);
		r.set(-0.6);
	}
	
	public static void stop() {
		l.set(0);
		r.set(0);
	}
	
	public static void stopAll() {
		l.set(0);
		r.set(0);
		r_in.set(0);
		lift.set(0);
	}
	
	public static void slowForward() {
		l.set(0.4);
		r.set(-0.4);
	}
	
	public static void slowBack() {
		r.set(0.3);
		l.set(-0.37);
	}
}
