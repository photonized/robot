package org.usfirst.frc6331.sabotage.autonomous;

import org.usfirst.frc6331.sabotage.Constants;
import org.usfirst.frc6331.sabotage.autonomous.Commands;

import edu.wpi.first.wpilibj.AnalogGyro;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.Ultrasonic;
import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.interfaces.Gyro;


/**
 * This class is really bad. By really bad, I mean, it works... but I'm not proud of it.
 * To improve this class, put sensors on the robot! This way, you'll be able to develop what's
 * commented out and actually use it. For auto, make different classes for turning, lifting, etc.
 * Don't use this class for everything. It's clunky, and overall really annoying to use, practically.
 * I might work on that later on. This is mainly time-based and needs a few hours of testing
 * and fine-tuning for it to work. What I mean by splitting into classes, is develop a function
 * to turn 90 degrees, or go forward for x steps, put it into a class, and then whenever you need
 * to turn right 90 degrees, you'll be able to use that class instead of rewriting it. Isn't this fantastic?
 * @author Vlad
 *
 */
public class Auto {
	
	
	/**
	 * Motors. Every motor is initially called in the Constants class. Here, I'm just creating variables
	 * that are much shorter, and more local, therefore easier to use. lB is leftBack, etc etc.
	 * You should be able to tell what everything is if you look at what I'm calling on the right side of the
	 * equals sign.
	 */
	static Victor lB = Constants.LEFT_BACK_DRIVE;
	static Victor lF = Constants.LEFT_FRONT_DRIVE;
	static Victor rB = Constants.RIGHT_BACK_DRIVE;
	static Victor rF = Constants.RIGHT_FRONT_DRIVE;
	static Victor claw = Constants.OPENER;
	static Victor r_in = Constants.RIGHT_CLAW;
	static Victor lift = Constants.LIFT;
	//static Victor l_in = Constants.LEFT_CLAW;
	
	
	/**
	 * Some fantastic feature was implemented this year, allowing the regrouping of motors and speed controllers into one
	 * group. This is nice, because instead of having to control every motor indivitually, you can take a group, like I did below, regrouping the left
	 * side of the drive train as well as the right side. Overall, this is not necessary, but it eases the coding process. I named them l and r, but they can be 
	 * refactored to anything you want to call them. Just select the word, right-click it, and Refactor->Rename. You can rename any vartiable globally this way.
	 */
	static SpeedControllerGroup l = new SpeedControllerGroup(lB, lF);
	static SpeedControllerGroup r = new SpeedControllerGroup(rB, rF);
	
	
	/**
	 * Encoders. We don't have these yet! Once we do, we're going to have to fine-tune the pulses so that one pulse is equal to some unit of measure, like an inch.
	 * This will improve the efficiency and the accuracy of everything.
	 */
	/*static Encoder eL = Constants.LEFT_ENCODER;
	static Encoder eR = Constants.RIGHT_ENCODER;
	
	static  double getAvgPosition() {
		return (eL.getDistance() + eR.getDistance()) / 2;
	}
	
	public static double kPulsesPerRevolution = 1024;
	public static double kDistancePerRevolution = 20;
	public static double kDistancePerPulse = kDistancePerRevolution / kPulsesPerRevolution;*/
	
	
	
	/**
	 * Gyro. We don't have this either. Get a larger board instead of the bad gyro we have on right now (it doesn't work).
	 */
	static Gyro gyro;;
	static DifferentialDrive drive;
	static double Kp = 0.03;
	//static double angle = gyro.getAngle();
	
	
	/**
	 * Uncommented, this initializes the gyros, adds an arcade drive function to easily control the bot during auto (DifferentialDrive), and sets the pulses for 
	 * eL and eR, which are the right and left encoders (encoderLeft, encoderRight).
	 */
	/*public static void init() {
		gyro = new AnalogGyro(1);
		
		drive = new DifferentialDrive(l, r);
		
		drive.setExpiration(0.1);
		
		eR.setDistancePerPulse(kDistancePerPulse);
		eL.setDistancePerPulse(kDistancePerPulse);
	*/
	//}
	
	/**
	 * Pretty self explanatory. resets the encoders and gyro to 0 when auto starts. Make sure to use this function in robotInit() to make sure that it's all at 0.
	 */
	public static void reset() {
		//eR.reset();
		//eL.reset();
		//gyro.reset();
	}
	
	/**
	 * Is technically supposed to drive straight. I haven't tested this and it's purely hypothetical.
	 */
	/*public static void straightTest() {
		reset();
		do {
			drive.arcadeDrive(0.5, angle * Kp);
		} while (getAvgPosition() < 50);
		drive.arcadeDrive(0, 0);
	}*/
	
	/**
	 * Should drive forward, turn 90 degrees, and go forward again while throwing a crate. This won't work, because we haven't tested it and I don't know how much
	 * one unit of measure in an encoder is as of now, since we have never tried them.
	 */
	/*public static void shortRight() {
	    reset();
	    do {
	    	drive.arcadeDrive(0.5, -angle * Kp);
	    } while (getAvgPosition() < 50.0);
	    reset();
	    do {
	    	drive.arcadeDrive(0.5, -90);
	    } while (eR.getDistance() < 10);
	    reset();
	    do {
	    	drive.arcadeDrive(0.5, -angle * Kp);
	    } while (getAvgPosition() < 10);
	    	drive.arcadeDrive(0, 0);
	    	r_in.set(1.0);
	    	l_in.set(-1.0);
	}*/
	/**
	 * Go straight only using a gyro. Time based.
	 */
	/*public static void straightGyro() {
		try {
			//drive.arcadeDrive(0.0, 0);
			l.set(0);
			r.set(0);
			Thread.sleep(5000);
			l.set(0.67);
			r.set(-0.6);
			//drive.arcadeDrive(1.0, -angle * Kp);
			Thread.sleep(1300);
			//drive.arcadeDrive(0.0, 0);
			l.set(0);
			r.set(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}*/
	
	/**
	 * Forward, turn right, go forward, drop a brick, go back, lower lift. This is where it starts getting bad and annoying. Later on I might split these into seperate functions
	 * but it still won't be practical since this is a try catch function and I really hate those due to limited functionality.
	 */
	public static void left() {
		try {
			
			Commands.forward();
			Thread.sleep(1500);
			lift.set(0.7);
			Thread.sleep(700);
			Commands.turnRight();
			lift.set(0.7);
			Thread.sleep(1100);
			Commands.slowForward();
			lift.set(0.5);
			Thread.sleep(1400);
			Commands.stop();
			lift.set(0.3);
			r_in.set(-1.0);
			Thread.sleep(3000);
			r_in.set(0);
			Commands.slowBack();
			Thread.sleep(500);
			Commands.stopAll();
		}
			catch(InterruptedException e) {
				e.printStackTrace();
			}
			
			
		}
	/**
	 * Same thing as above but for the right side.
	 */
	public static void right() {
	try {
		Commands.stop();
		Thread.sleep(0);
		Commands.forward();
		Thread.sleep(1450);
		lift.set(0.7);
		Thread.sleep(600);
		Commands.turnLeft();
		lift.set(0.7);
		Thread.sleep(550);
		Commands.slowForward();
		lift.set(1.0);
		Thread.sleep(500);
		Commands.stop();
		lift.set(0.3);
		r_in.set(-1.0);
		Thread.sleep(3000);
		r_in.set(0);
		Commands.slowBack();
		Thread.sleep(500);
		Commands.stopAll();
	}
		catch(InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Should go around all the way from the right side to the left behind the switch and drop it into the left switch.
	 */
	public static void longRight() {
		try {
		Commands.forward();
		Thread.sleep(2900);
		Commands.turnLeft();
		Thread.sleep(715);
		Commands.forward();
		lift.set(0.74);
		Thread.sleep(1950);
		Commands.turnLeft();
		lift.set(0.8);
		Thread.sleep(500);
		Commands.slowForward();
		lift.set(0.3);
		Thread.sleep(600);
		Commands.stop();
		r_in.set(-1.0);
		Thread.sleep(1000);
		r_in.set(0);
		Commands.slowBack();
		lift.set(0);
		Thread.sleep(500);
		Commands.stopAll();
		}
		catch (InterruptedException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Same thing as above but for the left side and right switch.
	 */
	public static void longLeft() {
		
	}
	
	/**
	 * Middle, goes to the right and drops a cube (maybe).
	 */
	public static void midRight() {
	try {
		Commands.stopAll();
		Thread.sleep(3000);
		Commands.forward();
		lift.set(0.750);
		Thread.sleep(1200);
		Commands.turnLeft();
		Thread.sleep(220);
		Commands.forward();
		Thread.sleep(700);
		Commands.stop();
		lift.set(0.3);
		r_in.set(-1.0);
		Thread.sleep(1000);
		Commands.slowBack();
		lift.set(0);
		r_in.set(0);
		Thread.sleep(500);
		Commands.stopAll();
	} catch(InterruptedException e) {
		e.printStackTrace();
	}
}
	
	/**
	 * Middle, goes to the left and drops a cube.
	 */
	public static void midLeft() {
	try {
		Commands.stopAll();
		Thread.sleep(3000);
		Commands.turnLeft();
		Thread.sleep(450);
		Commands.forward();
		lift.set(0.7);
		Thread.sleep(1300);
		Commands.turnRight();
		Thread.sleep(300);
		Commands.forward();
		Thread.sleep(900);
		Commands.stop();
		lift.set(0.3);
		r_in.set(-1.0);
		Thread.sleep(1000);
		Commands.slowBack();
		lift.set(0);
		r_in.set(0);
		Thread.sleep(500);
		Commands.stopAll();
	} catch (InterruptedException e){
		e.printStackTrace();
		}
	}
	
	public static void straight() {
		try {
			Commands.forward();
			Thread.sleep(1500);
			Commands.stopAll();
		} catch (InterruptedException e) {
		e.printStackTrace();
		}
	}
}

	    
