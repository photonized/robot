package org.usfirst.frc6331.sabotage.mechanics;

import org.usfirst.frc6331.sabotage.Constants;


public class Claw {
	public static void clawInit() {
		
		/**
		 * Calling the buttons.
		 */
		double rT = Constants.ACTION_STICK.getRawAxis(2);
		double lT = Constants.ACTION_STICK.getRawAxis(3);
		
		/**
		 * Local variables.
		 */
		double left = 0.0;
		double right = 0.0;
		
		/**
		 * Motor logic.
		 */
		
		if (rT > 0) {
			right = rT;
			left = -rT;
		}
		
		if(lT > 0) {
			right = -lT;
			left = lT;
		}
		
		Constants.RIGHT_CLAW.set(right);
		//Constants.LEFT_CLAW.set(left);
	}
}
