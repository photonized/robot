package org.usfirst.frc6331.sabotage.mechanics;

import org.usfirst.frc6331.sabotage.Constants;

public class Drive {
    
    public static void driveInit() {
    	
    	
        /**
         * The buttons being called.
         */
    	double leftXUnrounded = (Constants.DRIVE_STICK.getRawAxis(0));
    	double leftX = Math.round(leftXUnrounded * 10.0) / 10.0;
    	double rT = (Math.pow(Constants.DRIVE_STICK.getRawAxis(3), 3));
    	double lT = (Math.pow(Constants.DRIVE_STICK.getRawAxis(2), 3));
    	boolean aButton = Constants.DRIVE_STICK.getRawButton(1);
    	boolean xButton = Constants.DRIVE_STICK.getRawButton(3);
    	double left =  0.0;
    	double right =  0.0;
		
	/**
	 * Driving logic.
	 */
    if(aButton) {
    	right = -0.2 + (leftX * 0.3);
    	left = -0.2 - (leftX * 0.3);
    }
    
    if(xButton) {
    	right = 0.2 + (leftX * 0.3);
    	left = 0.2 - (leftX * 0.3);
    }
    
	if(rT != 0) {
			right = (Constants.DRIVE_SPEED * -rT) + (leftX * 0.3);
			left = (Constants.DRIVE_SPEED * -rT) - (leftX * 0.3);
		}
	if(lT != 0) {
			right = (Constants.DRIVE_SPEED * lT) + (leftX * 0.3);
			left = (Constants.DRIVE_SPEED * lT) - (leftX * 0.3);
		}
		
	/**
	 * Setting the local variable to the constant.
	 */
	Constants.LEFT_FRONT_DRIVE.set(-left);
    Constants.LEFT_BACK_DRIVE.set(-left);
    Constants.RIGHT_FRONT_DRIVE.set(right);
    Constants.RIGHT_BACK_DRIVE.set(right);
    }
    
}
