package org.usfirst.frc6331.sabotage.mechanics;

import org.usfirst.frc6331.sabotage.Constants;

public class Lift {
	public static void liftInit() {
		
		/**
		 * Calling the buttons.
		 */
		boolean a = Constants.ACTION_STICK.getRawButton(2);
		boolean y = Constants.ACTION_STICK.getRawButton(4);
		
		/**
		 * Local variable.
		 */
		double liftSpeed = -0.3;
		
		if(a) {
			liftSpeed = 0.1;
		}
		
		
		
		if(y) {
			liftSpeed = -0.9;
		}
		
		Constants.LIFT.set(-liftSpeed);
		
	}
}
