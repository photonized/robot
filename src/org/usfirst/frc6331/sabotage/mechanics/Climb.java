package org.usfirst.frc6331.sabotage.mechanics;

import org.usfirst.frc6331.sabotage.Constants;

public class Climb {
	public static void climbInit() {
		
		/**
		 * Calling the buttons. In constants, raised to the power of 3 for less sensitivity. Can be expanded horizontally if you want.
		 */
		double rightStick = (Math.pow(Constants.DRIVE_STICK.getRawAxis(5), 3));
		
		/**
		 * Local variables. The original speed is always 0 because when you aren't pressing anything, you don't want the motors to be running.
		 */
		double liftSpeed = 0.0;
		
		
		/**
		 * If the right stick is less than this or more than this, lift speed equals the negative of the right stick value. No other way to explain this.
		 */
		if(rightStick < -0.2 || rightStick > 0.2) {
			liftSpeed = -rightStick;
		}
		
		/**
		 * Setting the motor to the local variable, because it's impractical to call Constants.CLIMB.set(#), instead of just liftSpeed(#).
		 */
		Constants.CLIMB.set(-liftSpeed);		
	}
}
